package com.enparadigm.enalyticssample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.enparadigm.enalytics.Enalytics;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String EVENT_URL = "http://www.YOUR_HOST.com/setevents/";
    private static final String PROPERTY_URL = "http://www.YOUT_HOST.com/setproperties";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * How to use Enalytics library
         */
        //initialize Enalytics
        Enalytics enalytics = Enalytics.getInstance(getApplicationContext()).initialize(1, "9999999999", "123", EVENT_URL, PROPERTY_URL);

        //set user properties
        enalytics.getPeople().set("name", "Krishna");
        enalytics.getPeople().set("email", "krishna.kumar@enparadigm.com");

        //send events
        JSONObject props = null;
        try {
            props = new JSONObject();
            props.put("name", "home");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        enalytics.track("current_page", props);
    }
}
