package com.enparadigm.enalytics.api;

import com.enparadigm.enalytics.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final Object LOCK = new Object();
    private final Retrofit retrofit;
    private static ApiClient sInstance;

    public static Retrofit getApiClient() {
        synchronized (LOCK) {
            if (sInstance == null) {
                sInstance = new ApiClient();
            }
            return sInstance.retrofit;
        }
    }

    private ApiClient() {
        OkHttpClient okHttpClient;
        okHttpClient = getNormalOkHttpClient();
        retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient getNormalOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }
}
