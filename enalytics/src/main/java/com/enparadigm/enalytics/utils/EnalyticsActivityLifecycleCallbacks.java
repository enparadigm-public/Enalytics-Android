package com.enparadigm.enalytics.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.enparadigm.enalytics.Enalytics;
import com.enparadigm.enalytics.interfaces.AutoEvents;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class EnalyticsActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    private final Enalytics mEnalyticsInstance;
    private final EnConfig mEnConfig;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Runnable check;
    private boolean mIsForeground = true;
    private boolean mPaused = true;
    private static Double sStartSessionTime;
    public static final int CHECK_DELAY = 500;

    public EnalyticsActivityLifecycleCallbacks(Enalytics enalytics, EnConfig config) {
        mEnalyticsInstance = enalytics;
        mEnConfig = config;
        if (sStartSessionTime == null) {
            sStartSessionTime = (double) System.currentTimeMillis();
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityPaused(final Activity activity) {
        mPaused = true;
        if (check != null) {
            mHandler.removeCallbacks(check);
        }

        mHandler.postDelayed(check = new Runnable() {
            @Override
            public void run() {
                if (mIsForeground && mPaused) {
                    mIsForeground = false;
                    try {
                        double sessionLength = System.currentTimeMillis() - sStartSessionTime;
                        if (sessionLength >= mEnConfig.getMinimumSessionDuration() && sessionLength < mEnConfig.getSessionTimeoutDuration()) {
                            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
                            nf.setMaximumFractionDigits(1);
                            String sessionLengthString = nf.format((System.currentTimeMillis() - sStartSessionTime) / 1000);
                            JSONObject sessionProperties = new JSONObject();
                            sessionProperties.put(AutoEvents.SESSION_LENGTH, sessionLengthString);
                            mEnalyticsInstance.track(AutoEvents.SESSION, sessionProperties);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mEnalyticsInstance.onBackground();
                }
            }
        }, CHECK_DELAY);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        mPaused = false;
        boolean wasBackground = !mIsForeground;
        mIsForeground = true;

        if (check != null) {
            mHandler.removeCallbacks(check);
        }

        if (wasBackground) {
            // App is in foreground now
            sStartSessionTime = (double) System.currentTimeMillis();
            mEnalyticsInstance.onForeground();
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    protected boolean isInForeground() {
        return mIsForeground;
    }
}
