package com.enparadigm.enalytics.utils;

import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.enparadigm.enalytics.db.DBHelper;

/**
 * Created by krishna on 06/07/18.
 */

public class Worker {
    private static final String TAG = "Worker";
    private static final Object WORKER_LOCK = new Object();
    private EnalyticsHandler handler;

    public Worker(DBHelper dbHelper, EnConfig enConfig) {
        if (handler == null) {
            handler = startWorkerThread(dbHelper, enConfig);
        }
    }

    private EnalyticsHandler startWorkerThread(DBHelper dbHelper, EnConfig enConfig) {
        HandlerThread handlerThread = new HandlerThread("enalytics_worker_thread", Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        return new EnalyticsHandler(enConfig, dbHelper, handlerThread.getLooper());
    }

    public void sendMessage(Message msg) {
        synchronized (WORKER_LOCK) {
            if (handler == null) {
                Log.e(TAG, "Worker thread died. Can not enqueue any more messages");
            } else {
                handler.sendMessage(msg);
            }
        }
    }
}
