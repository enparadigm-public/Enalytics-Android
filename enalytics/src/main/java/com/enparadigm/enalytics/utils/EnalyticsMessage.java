package com.enparadigm.enalytics.utils;

/**
 * Created by krishna on 06/07/18.
 */

public class EnalyticsMessage {
    public static final int TYPE_EVENTS = 1;
    public static final int TYPE_PEOPLE_PROPERTY = 2;
    public static final int TYPE_FLUSH = 3;
}
