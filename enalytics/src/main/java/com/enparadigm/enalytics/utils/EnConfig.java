package com.enparadigm.enalytics.utils;

/**
 * Created by krishna on 09/07/18.
 */

public class EnConfig {
    private String uniqueueId;
    private String distinctId;
    private boolean isOptedOutOfTracking;
    private int appId;
    private double minimumSessionDuration;
    private double sessionTimeoutDuration;
    private String eventsUrl;
    private String propertiesUrl;
    private int eventsBatchLimit = 200;

    public String getUniqueueId() {
        return uniqueueId;
    }

    public void setUniqueueId(String uniqueueId) {
        this.uniqueueId = uniqueueId;
    }

    public boolean isOptedOutOfTracking() {
        return isOptedOutOfTracking;
    }

    public void setOptedOutOfTracking(boolean optedOutOfTracking) {
        isOptedOutOfTracking = optedOutOfTracking;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public int getAppId() {
        return appId;
    }

    public double getMinimumSessionDuration() {
        return minimumSessionDuration;
    }

    public double getSessionTimeoutDuration() {
        return sessionTimeoutDuration;
    }

    public void setMinimumSessionDuration(double minimumSessionDuration) {
        this.minimumSessionDuration = minimumSessionDuration;
    }

    public void setSessionTimeoutDuration(double sessionTimeoutDuration) {
        this.sessionTimeoutDuration = sessionTimeoutDuration;
    }

    public String getEventsUrl() {
        return eventsUrl;
    }

    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    public String getPropertiesUrl() {
        return propertiesUrl;
    }

    public void setPropertiesUrl(String propertiesUrl) {
        this.propertiesUrl = propertiesUrl;
    }

    public String getDistinctId() {
        return distinctId;
    }

    public void setDistinctId(String distinctId) {
        this.distinctId = distinctId;
    }

    public int getEventsBatchLimit() {
        return eventsBatchLimit;
    }
    public void setEventsBatchLimit(int eventsBatchLimit) {
        this.eventsBatchLimit = eventsBatchLimit;
    }
}
