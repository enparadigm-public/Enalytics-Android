package com.enparadigm.enalytics.utils;

import android.os.Message;
import android.text.TextUtils;

import com.enparadigm.enalytics.interfaces.UserProperty;
import com.google.gson.JsonArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by krishna on 06/07/18.
 */
public class UserPropertyImp implements UserProperty {
    private EnConfig enConfig;
    private Worker worker;

    public UserPropertyImp(EnConfig enConfig, Worker worker) {
        this.enConfig = enConfig;
        this.worker = worker;
    }

    @Override
    public void set(String propertyName, Object value) {
        if (TextUtils.isEmpty(propertyName)) return;
        try {
            set(new JSONObject().put(propertyName, value));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setMap(Map<String, ? extends Object> properties) {
        if (properties == null) return;
        try {
            set(new JSONObject(properties));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void set(JSONObject properties) {
        if (enConfig.isOptedOutOfTracking() || properties == null) return;
        try {
            JSONObject messageObject = new JSONObject();
            messageObject.put("set", properties);
            messageObject.put("user_id", enConfig.getUniqueueId());
            messageObject.put("distinctid", enConfig.getDistinctId());
            messageObject.put("timestamp", System.currentTimeMillis());
            //enqueue the properties
            Message message = Message.obtain();
            message.what = EnalyticsMessage.TYPE_PEOPLE_PROPERTY;
            message.obj = messageObject;
            worker.sendMessage(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnce(String propertyName, Object value) {
        if (TextUtils.isEmpty(propertyName)) return;
        try {
            setOnce(new JSONObject().put(propertyName, value));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnceMap(Map<String, ? extends Object> properties) {
        if (properties == null) return;
        try {
            setOnce(new JSONObject(properties));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnce(JSONObject properties) {
        if (enConfig.isOptedOutOfTracking() || properties == null) return;
        try {
            final JSONObject messageObject = new JSONObject();
            messageObject.put("set_once", properties);
            messageObject.put("user_id", enConfig.getUniqueueId());
            messageObject.put("distinctid", enConfig.getDistinctId());
            messageObject.put("timestamp", System.currentTimeMillis());
            //enqueue the properties
            Message message = Message.obtain();
            message.what = EnalyticsMessage.TYPE_PEOPLE_PROPERTY;
            message.obj = messageObject;
            worker.sendMessage(message);
        } catch (final JSONException e) {
        }
    }

    @Override
    public void increment(String name, double increment) {
        if (TextUtils.isEmpty(name)) return;
        Map<String, Double> propertyMap = new HashMap<>();
        propertyMap.put(name, increment);
        try {
            increment(propertyMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void increment(Map<String, ? extends Number> properties) {
        if (enConfig.isOptedOutOfTracking() || properties == null) return;
        try {
            final JSONObject messageObject = new JSONObject();
            messageObject.put("add", new JSONObject(properties));
            messageObject.put("user_id", enConfig.getUniqueueId());
            messageObject.put("distinctid", enConfig.getDistinctId());
            messageObject.put("timestamp", System.currentTimeMillis());
            //enqueue the properties
            Message message = Message.obtain();
            message.what = EnalyticsMessage.TYPE_PEOPLE_PROPERTY;
            message.obj = messageObject;
            worker.sendMessage(message);
        } catch (final JSONException e) {
        }
    }

    @Override
    public void unset(String name) {
        if (enConfig.isOptedOutOfTracking() || TextUtils.isEmpty(name)) return;
        try {
            JSONObject messageObject = new JSONObject();
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(name);
            messageObject.put("unset", jsonArray);
            messageObject.put("user_id", enConfig.getUniqueueId());
            messageObject.put("distinctid", enConfig.getDistinctId());
            messageObject.put("timestamp", System.currentTimeMillis());
            //enqueue the properties
            Message message = Message.obtain();
            message.what = EnalyticsMessage.TYPE_PEOPLE_PROPERTY;
            message.obj = messageObject;
            worker.sendMessage(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
