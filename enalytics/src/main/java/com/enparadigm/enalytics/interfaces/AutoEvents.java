package com.enparadigm.enalytics.interfaces;

/**
 * Created by krishna on 06/07/18.
 */

public interface AutoEvents {
    String FIRST_OPEN = "first_open";
    String SESSION = "session";
    String SESSION_LENGTH = "session_length";
    String APP_UPDATED = "updated";
    String VERSION_UPDATED = "updated_version";
}
