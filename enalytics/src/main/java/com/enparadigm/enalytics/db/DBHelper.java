package com.enparadigm.enalytics.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.enparadigm.enalytics.db.model.MetaEvents;
import com.enparadigm.enalytics.db.model.MetaProperties;


/**
 * Created by krishna on 06/07/18.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final Object DATABASE_LOCK = new Object();
    private static final String DB_NAME = "enalytics.db";
    private static final int DB_VERSION = 1;
    private static DBHelper sInstance;

    public static DBHelper getInstance(Context context) {
        if (sInstance == null) {
            synchronized (DATABASE_LOCK) {
                if (sInstance == null) {
                    sInstance = new DBHelper(context, DB_NAME, null, DB_VERSION);
                }
            }
        }
        return sInstance;
    }

    private DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MetaProperties.CREATE_TABLE);
        db.execSQL(MetaEvents.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static boolean isFirstOpen(Context context) {
        return !context.getDatabasePath(DB_NAME).exists();
    }
}
